#!/bin/bash

#Popis: Skript depcg.sh vypisuje zavislosti mezi funkcemi. Byl vytvořen v rámci projektu do předmětu iOS na FIT VUT
#Autor: Jan Herec
#Vytvořeno dne: 15. 3. 2014
#Naposledy upraveno dne: 25. 3. 2014

#Vychozi hodnoty promennych, ktere mohou byt prenastaveny dle zadanych prepinacu
#Tyto promenne jsou ridici, tedy na zaklade jejich hodnoty provadime urcite akce
ignoreGraph=true
ignorePLT=true
ignoreCalle=true
ignoreCaller=true

#Vychozi vzory, ktere budou dosazeny do regularnich vyrazu pro hledani konkretnich objektu
#Tyto obecne vzory, mohou byt nahrazeny konkretnimi hodnotami, dle zadanych prepinacu
calle=".+"
caller=".+"

#Zpracujeme zadane prepinace, to v podstate znamena, ze nastavime jiné hodnoty vyse uvedenym promennym
while getopts :gpr:d: o
do
  case "$o" in
  #Zapne vykresovani grafu zavislosti
  g) ignoreGraph=false;;
  #Zapne vypisovani funkci ve sdilenych knihovnach
  p) ignorePLT=false;;
  #Zapne vypisovani zavislosti pro konkretni (volanou) funkci
  r) ignoreCalle=false
     calle=$OPTARG;;
  #Zapne vypisovani zavislosti pro konkretni (volajici) funkci
  d) ignoreCaller=false
     caller=$OPTARG;;
  *) echo -e "Chyba!\nNebyly zadány přepínače a jejich argumenty v požadovaném formátu.\n" \
           "Skript očekává přepínače a argumenty v následujícím formátu: depcg.sh [-g] [-p] [-r FUNCTION_ID|-d FUNCTION_ID] FILE" >&2
     exit 1;;
  esac
done
#Definovat muzeme bud volajici funkci nebo volanou funkci, nikoliv obe dve
if test $ignoreCalle = false -a $ignoreCaller = false; then
  echo -e "Chyba!\nZadali jste přepínače -r a -d. To není dovoleno. Zadejte buď jeden nebo druhý." >&2
  exit 1
fi

#Posuneme argumenty predane skriptu o hodnotu ridici promenne OPTIND, ktera ukazuje na aktualne zpracovavany argument
#To nam umozni jednoduse pracovat s argumenty zadanymi za prepinaci
((OPTIND--))
shift $OPTIND

#Pokud po prepinacich nenasleduje alespon jeden argument (= soubor) nema cenu dal pokracovat
if test "$1" = ""; then
  echo -e "Chyba!\nNepředali jste jako argument žádný soubor." >&2
  exit 1
elif test "$2" != ""; then
  echo -e "Chyba!\nPředali jste více argumentů než je povoleno." >&2
  exit 1
fi

#Zkontrolujeme, zdali soubory vyhovuji formatu, ktery vyzaduje program objdump.
objdump -d -j .text $1 > /dev/null 2>&1
if test $? -ne 0; then
  echo -e "Chyba!\nNěkterý ze souborů buď neexistuje, nebo nebo má neplatný formát pro zpracování programem objdump." >&2
  exit 1
fi

#Pokud nechceme ve vypisu funkce ve sdilenych knihovnach, tak je z vypisu vynechame
if test $ignorePLT = true; then
  #Do promenne si ulozime castecne zpracovane data z programu objdump
  seznamFunkci=`objdump -d -j .text "$1" | \
  awk '/^[0-9a-z]+ <.+>:$/ { print $2 } /^.*callq +[0-9a-z]+ <[^+]+>$/ { print $9 }' | tr -d "<>" | grep -E -v '^.+@plt$'`
else
  seznamFunkci=`objdump -d -j .text "$1" | \
  awk '/^[0-9a-z]+ <.+>:$/ { print $2 } /^.*callq +[0-9a-z]+ <[^+]+>$/ { print $9 }' | tr -d "<>"`
fi

#Vytvorime docasny soubor pro pomocne ukladani dat
tempFile=`mktemp '/tmp/$xherec00.XXXXXX'`

#Budeme cyklem projizdet seznam funkci a vypisovat do souboru zavislost mezi volajici a volanou funkci
for funkce in $seznamFunkci ; do
  #Zde zjistujeme jestli aktualne projizdena funkce je funkce volajici
  echo $funkce | grep -E '^.+:$' > /dev/null
  #Pokud se jedna o volajici funkci (zjistime podle navratoveho kodu posledniho prikazu egrep),
  #tak ji budeme pouzivat jako predponu pro dalsi volane funkce
  if test $? -eq 0; then
    opakujiciSe=`echo $funkce | sed 's/://'`
  else
    #Jinak zapiseme do souboru zavislost mezi caller a callee
    echo $opakujiciSe $funkce >> $tempFile
  fi
done

#Pokud nechceme vypis zavislosti v grafickem formatu
if test $ignoreGraph = true; then
  sort $tempFile | uniq | sed 's/ / -> /' | grep -E "^$caller -> ${calle}$"
else
  #Vypisujeme zavislosti v grafickem formatu
  {
    echo 'digraph CG {'
    sort $tempFile | uniq | sed -e 's/ / -> /' -e 's/$/;/' -e 's/@plt/_PLT/g' | grep -E "^$caller -> ${calle}(_PLT)?;$"
    echo '}'
  }
fi

rm $tempFile
exit 0

#Odchytame signaly
trap `rm $tempFile; exit 1` 1
trap `rm $tempFile; exit 1` 2
trap `rm $tempFile; exit 1` 15

