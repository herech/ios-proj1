#!/bin/bash

#Popis: Skript depsym.sh vypisuje zavislosti mezi soubory. Byl vytvořen v rámci projektu do předmětu iOS na FIT VUT
#Autor: Jan Herec
#Vytvořeno dne: 18. 3. 2014
#Naposledy upraveno dne: 25. 3. 2014

#Vychozi hodnoty promennych, ktere mohou byt prenastaveny dle zadanych prepinacu
#Tyto promenne jsou ridici, tedy na zaklade jejich hodnoty provadime urcite akce
ignoreGraph=true
ignoreObj2=true
ignoreObj1=true

#Vychozi vzory, ktere budou dosazeny do regularnich vyrazu pro hledani konkretnich objektu
#Tyto obecne vzory, mohou byt nahrazeny konkretnimi hodnotami, dle zadanych prepinacu
obj2=".+"
obj1=".+"

#Zpracujeme zadane prepinace, to v podstate znamena, ze nastavime jiné hodnoty vyse uvedenym promennym
while getopts :gr:d: o
do
  case "$o" in
  #Zapne vykreslovani grafu zavislosti
  g) ignoreGraph=false;;
  #Zapne vypisovani zavislosti pro konkretni (definujici) objekt a nastavi vzor pro hledani konkretniho objektu
  r) ignoreObj2=false
     obj2="(.*/)?$OPTARG";;
  #Zapne vypisovani zavislosti pro konkterni (zavisejici) objekt a nastavi vzor pro hledani konkretniho objektu
  d) ignoreObj1=false
     obj1="(.*/)?$OPTARG";;
  #Pokud nebyly zadany prepinace v pozadovanem formatu
  *) echo -e "Chyba!\nNebyly zadány přepínače a jejich argumenty v požadovaném formátu.\n" \
           "Skript očekává přepínače a argumenty v následujícím formátu: depsym.sh [-g] [-r|-d OBJECT_ID] FILEs" >&2
     exit 1;;
  esac
done

#Definovat muzeme bud definujici objekt nebo zavisejici objekt, nikoliv oba dva
if test $ignoreObj1 = false -a $ignoreObj2 = false; then
  echo -e "Chyba!\nZadali jste přepínače -r a -d. To není dovoleno. Zadejte buď jeden nebo druhý." >&2
  exit 1
fi

#Posuneme argumenty predane skriptu o hodnotu ridici promenne OPTIND, ktera ukazuje na aktualne zpracovavany argument
#To nam umozni jednoduse pracovat s argumenty zadanymi za prepinaci
(( OPTIND-- ))
shift $OPTIND

#Pokud po prepinacich nenasleduje alespon jeden argument (= soubor) nema cenu dal pokracovat
if test "$1" = ""; then
  echo -e "Chyba!\nNepředali jste jako argument žádný soubor." >&2
  exit 1
fi

#Naplnime pole seznamSouboru soubory, ktere byly predane jako argumenty skriptu
seznamSouboru=$*

#Zkontrolujeme, zdali soubory vyhovuji formatu, ktery vyzaduje program nm.
nm $* > /dev/null 2>&1
if test $? -ne 0; then
  echo -e "Chyba!\nNěkterý ze souborů buď neexistuje, nebo nebo má neplatný formát pro zpracování programem nm." >&2
  exit 1
fi

#Vytvorime docasny soubor pro pomocne ukladani dat a take vytvorime asociatovni pole
#V tomto poli budou klice budou predstavovany jmena souboru,ktere byly zadany jako argumenty
#A hodnoty budou reprezentovat jmena souboru, ale upravena tak, ze vyhovi pro výstupní formát grafu
tempFile=`mktemp '/tmp/$xherec00.XXXXXX'`

#Asociativni pole, kde klice predstavuji puvodni nazvy souboru a hodnoty predstavuji jejich upravene nazvy,
#ktere je nezbytne upravit pro graficky format
declare -A puvodniAZmeneneSoubory

#Asociativni pole, kde klice predstavuji nazev souboru a hodnoty predstavuji symboly, ktere jsou v danych souborech nedefinovany
declare -A fileU
#Asociativni pole, kde klice predstavuji nazev souboru a hodnoty predstavuji symboly, ktere jsou v danych souborech definovany
declare -A fileD

#seznamSouboru1=${seznamSouboru[*]}
#Budeme postupne provadet akci pro kazdy soubor predany na vstupu jako argument
for soubor in $seznamSouboru ; do
  #Vytvorime si seznam symbolu, ktere jsou pro dany soubor (aktualne prochazeny cyklem) nedefinovane
  nedefSymbolyVSouboru=`nm $soubor | awk '/^.+U [^ @]+$/ { print $2 }'`

  #Pro kazdy nedefinovany symbol budeme provadet urcitou akci,
  #tedy vytvorime v asociativnim poli zavislost mezi nim a souborem, kde je pouzity ale nedefinovany
  i=0
  for nedefSymbol in $nedefSymbolyVSouboru ; do
    fileU[$soubor".$i"]=$nedefSymbol
    (( i++ ))
  done

  #Vytvorime si seznam symbolu, ktere jsou pro dany soubor (aktualne prochazeny cyklem) definovane
  defSymbolyVSouboru=`nm $soubor | awk '/^[a-z0-9]+ [TBCDG] [^ ]+$/ { print $3 }'` 

  #Pro kazdy definovany symbol budeme provadet urcitou akci,
  #tedy vytvorime v asociativnim poli zavislost mezi nim a souborem, kde je definovany
  for defSymbol in $defSymbolyVSouboru ; do
    fileD[$defSymbol]=$soubor
  done

done

fileUKeys=${!fileU[*]}

#Pro kazdy zavisly soubor (pouziva symbol, ktery sam nedefinoval) budeme hledat soubor, kde je dany symbol definovany
for zavislySoubor in $fileUKeys ; do
 #Pokud se nam povedlo najit symbol, ktery spojuje soubor ve kterem byl pouzit, ale nedefinovan a soubor, kde je definovan,
 #mame zavislost, kterou muzeme dale zpracovat
   symbol=${fileU[$zavislySoubor]}

   if test ${fileD[$symbol]} ; then
     #Zde odstranime posledni ciselny znak v nazvech souboru
     #(bylo treba jej tam dat, protoze by jinak dane nazvy souboru nebyly jako klice unikatni)
     zavislySoubor=`echo $zavislySoubor | sed -r 's/^(.+)\.[0-9]+$/\1/'`
     nezavislySoubor=`echo ${fileD[$symbol]}`

     #V tomto bloku (tedy je jedno jestli ve vetvi if nebo else) budeme zapisovat do tempSouboru zavislosti mezi soubory,
     #dle dane vetve if, nebo else budou tyto zavislosti v ruznych formatech
     if test $ignoreGraph = false; then
       #Pro graficky format bude potreba upravit nazvy souboru a ulozit do pole puvodniAZmeneneSoubory dvojici: puvodniNazev => novyNazev
       zavislySoubor=`echo $zavislySoubor | sed -r 's/^(.*\/)?([^(\/)]+)$/\2/'`
       editovanyZavislySoubor=`echo $zavislySoubor | tr "-" "_" | tr "." "D" | tr "+" "P"`
       puvodniAZmeneneSoubory[$zavislySoubor]=$editovanyZavislySoubor
       nezavislySoubor=`echo $nezavislySoubor | sed -r 's/^(.*\/)?([^(\/)]+)$/\2/'`
       editovanyNezavislySoubor=`echo $nezavislySoubor | tr "-" "_" | tr "." "D" | tr "+" "P"`
       puvodniAZmeneneSoubory[$nezavislySoubor]=$editovanyNezavislySoubor

       echo "$editovanyZavislySoubor -> $editovanyNezavislySoubor [label=\""$symbol"\"];" >> $tempFile
     else
      # exit 0
       echo "$zavislySoubor -> $nezavislySoubor ("$symbol")" >> $tempFile
     fi
   fi
done

#Pokud nechceme vypisovat data v grafickem formatu, tak je jednoduse vypiseme z tempSouboru s mensimi upravami
if test $ignoreGraph = true; then
  sort $tempFile | uniq | grep -E "^$obj1 -> $obj2 \(.+\)$"
else
  #Pokud chceme vypisovat jen ty zavislosti, ve kterych je zavisly objekt nebo definujici objekt s urcitym nazvem,
  #bude potreba upravit jeho nazev, aby vyhovoval grafickemu formatu
  if test $ignoreObj1 = false; then
    obj1=`echo $obj1 | tr "-" "_" | tr "." "D" | tr "+" "P" | sed -r 's/^(.*\/)?([^(\/)]+)$/\2/'`
  fi
  if test $ignoreObj2 = false; then
    obj2=`echo $obj2 | tr "-" "_" | tr "." "D" | tr "+" "P" | sed -r 's/^(.*\/)?([^(\/)]+)$/\2/'`
  fi

    #Vypiseme zavislosti v grafickem formatu
    {
      echo 'digraph GSYM {'
      sort $tempFile | uniq | grep -E "^$obj1 -> $obj2 \[.+\];$"

      #Soucasti vystupu grafickeho formatu je taky definice uzlu
      for key in ${!puvodniAZmeneneSoubory[*]} ; do
        sort $tempFile | uniq | grep -E "^$obj1 -> $obj2 \[.+\];$" | grep -E '^.*'${puvodniAZmeneneSoubory[$key]}'.* \[.+\];$' > /dev/null
        if test $? -eq 0; then
          echo ${puvodniAZmeneneSoubory[$key]} "[label=\""$key"\"];"
        fi
      done
      echo '}'
    }
fi

rm $tempFile
exit 0

#Odchytavame signaly
trap `rm $tempFile; exit 1` 1
trap `rm $tempFile; exit 1` 2
trap `rm $tempFile; exit 1` 15

